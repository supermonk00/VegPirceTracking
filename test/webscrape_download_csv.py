#### the code refer to https://www.webscrapingpro.tw/cnyes-fund-web-scraping-using-python/
# it enables to control google chrome, control the drop-down menu to show required data


# remember to install package....
import time
import datetime
from selenium import webdriver
import pandas as pd
from Functions import download_selected_date

if __name__ == "__main__":
    import glob
    # 不論使用者找出 chromedriver 路徑
    chrome_driver_path = glob.glob("../**/VegPirceTracking/chromedriver", recursive=True)[0]

    # 不論使用者找出 下載資料夾 路徑
    download_folder_path = glob.glob("../**/VegPirceTracking", recursive=True)[0]
    

    # 下載檔名
    File_name = 'Post_organized_test01.csv'
    
    df = download_selected_date(chrome_driver_path, download_folder_path, File_name, '2020/10/17', '2020/10/20')

    # 簡單看一下
    print(df.head())
    print(df.tail())
    print(df.describe(include='all', datetime_is_numeric=True))
    
    



