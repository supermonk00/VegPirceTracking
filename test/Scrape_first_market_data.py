# remember to install package....
import pandas as pd
import glob
from Functions import download_selected_date_from_single_market

if __name__ == "__main__":
    import glob
    # 不論使用者找出 chromedriver 路徑
    chrome_driver_path = glob.glob("../**/VegPirceTracking/chromedriver", recursive=True)[0]

    # 不論使用者找出 下載資料夾 路徑
    download_folder_path = glob.glob("../**/VegPirceTracking", recursive=True)[0]
    
    # 下載檔名
    download_filename = 'Post_organized_test.csv' 
    
    download_selected_date_from_single_market(chrome_driver_path, download_folder_path, download_filename, '2021/10/17', '2021/10/20')

    # 簡單看一下
    csv_full_path = download_folder_path + '/' + download_filename
    df = pd.read_csv(csv_full_path)
    print(df.head())
    print(df.tail())
