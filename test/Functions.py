def open_tab_for_single_market(chrome_driver_path):
    """
    This function opens a tab of the single market page
    """
    from selenium import webdriver
    import time
    url = 'http://www.tapmc.com.taipei/Pages/Trans/Price1'
    ### control google chrome
    driver = webdriver.Chrome(executable_path=chrome_driver_path) # set path to your directory which contains chromedriver
    driver.get(url=url)         
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DDL_FV_Code"]').click() # choose vegetable/fruit 
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DDL_Category"]').click() # choose single market
    driver.find_element_by_xpath('//*[@id="DDL_Category"]/option[2]').click() 
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DDL_Market"]').click() # choose the first market
    driver.find_element_by_xpath('//*[@id="DDL_Market"]/option[1]').click()
    return driver

def scrape_single_market_results(source, date):
    """
    This function use pandas to scrape single market data table and clean it, returns dataframe
    """
    import pandas as pd
    df = pd.read_html(source)[0] # scrape table
    df.columns = df.columns.droplevel(0) # drop multilevel columns
    df.drop([len(df)-1], inplace = True) # drop misread total
    df.drop([df.columns[-1]], axis=1, inplace=True) # drop last unnamed column
    df['日期'] = date
    return df

def download_selected_date_from_single_market(chrome_driver_path, download_folder_path, file_name, starting_date, ending_date=None):
    """
    This function downloads and returns a dataframe 
    containing vegetable auction price data 
    using webscraping for the given date range
    """

    date_list = generate_date_list(starting_date, ending_date)
    driver = open_tab_for_single_market(chrome_driver_path)

    for selected_date in date_list: # iter through all date
        print('downloading {} data ...'.format(selected_date)) # print message to let know its dowloading
        source = input_date_and_send(driver, selected_date=selected_date) # call function to scrape page
        if not source == None:
            df = scrape_single_market_results(source, selected_date)
            csv_path = download_folder_path + '/' + file_name
            df.to_csv(csv_path, mode='a', header=True, index=False)
            
    driver.close() # close the driver

def soup_to_dic(soup): 
    """this function turns soup into a dictionary"""
    import pandas as pd
    from bs4 import BeautifulSoup
    import numpy as np
    if soup == None:
        return {}
    else: 
        crops = []
        codes = []
        varieties = []
        count = 0 
        for name in soup.findAll('td',attrs = {'class': "text-center"}):
            count += 1 
            if count % 3  == 1:
                codes.append(name.text)
            if count % 3  == 2:
                crops.append(name.text)
            if count % 3  == 0:
                varieties.append(name.text)

        ceilling_prices = []
        moderate_prices = []
        floor_prices = []
        count = 0 
        for price in soup.findAll('td',attrs = {'class': "text-right"}):
            count += 1 
            if count % 3  == 1:
                if price.text == '':
                    ceilling_prices.append(np.nan)
                else:
                    ceilling_prices.append(float(price.text))
            if count % 3  == 2:
                if price.text == '':
                    moderate_prices.append(np.nan)
                else:
                    moderate_prices.append(float(price.text))
            if count % 3  == 0:
                if price.text == '':
                    floor_prices.append(np.nan)
                else:
                    floor_prices.append(float(price.text))


        d = {'code': codes[:-1], 'crop': crops, 'variety': varieties, 
            'ceilling price': ceilling_prices,'moderate price': moderate_prices,
            'floor price': floor_prices}

        return d

def input_date_and_send(driver, selected_date):
    """
    This function returns the page source of a given date
    """
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.common.exceptions import TimeoutException
    from selenium.webdriver.common.keys import Keys
    
    date_box = driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_txtDate"]') # 選日期框框
    date_box.clear() # 清除內容
    date_box.send_keys(selected_date) # 填入日期
    date_box.send_keys(Keys.ENTER) # 送出日期
    driver.find_element_by_xpath('//*[@id="ContentPlaceHolder1_btnQuery"]').click() # submit 查詢
    # time.sleep(3)
    try:
        WebDriverWait(driver, 0.3).until(EC.alert_is_present(),
                                   'Timed out waiting for PA creation ' +
                                   'confirmation popup to appear.')
        alert = driver.switch_to.alert
        alert.accept()
        print("No data today. Alert accepted")
    except TimeoutException:
        return driver.page_source

def generate_date_list(starting_date, ending_date):
    """
    This function generates a date list in ROC format given date in normal format(YYYY/MM/DD)
    """
    import datetime
    if ending_date == None: # if there is no input starting date then its today
        end = datetime.datetime.today()
    else:
        end = datetime.datetime.strptime(ending_date, '%Y/%m/%d') # make it in to datetime object
    
    start = datetime.datetime.strptime(starting_date, '%Y/%m/%d') # make it in to datetime object
    days = (end - start).days # calculate delta
    date_list = [(end - datetime.timedelta(days=x)).strftime("%Y/%m/%d") for x in range(days)] # generate date list for iterating download
    date_list_ROC = [str(int(x[0:4])-1911) + x[4:] for x in date_list]

    return date_list_ROC

def open_total_market_page(chrome_driver_path):
    """
    This function open a page for total market veg price, returns driver
    """
    from selenium import webdriver
    import time
    url = 'http://www.tapmc.com.taipei/Pages/Trans/Price1'
 
    ### control google chrome
    driver = webdriver.Chrome(executable_path=chrome_driver_path) # set path to your directory which contains chromedriver
    driver.get(url=url)         
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="DDL_FV_Code"]').click() # choose vegetable/fruit 
    time.sleep(1)
    return driver

def download_selected_date(chrome_driver_path, download_folder_path, file_name, starting_date, ending_date=None):
    """
    This function downloads and returns a dataframe 
    containing vegetable auction price data 
    using webscraping for the given date range
    """
    import pandas as pd
    date_list = generate_date_list(starting_date, ending_date)
    
    dic_main = {}

    driver = open_total_market_page(chrome_driver_path)

    for selected_date in date_list: # iter through all date
        print('downloading {} data ...'.format(selected_date)) # print message to let know its dowloading
        from bs4 import BeautifulSoup
        source = input_date_and_send(driver, selected_date=selected_date) # call function to scrape page
        if bool(source): # check if the source is empty
            soup = BeautifulSoup(source,features="lxml")
            dict_temp = soup_to_dic(soup) # call function to organize soup to dataframe
            dict_temp['date'] = len(dict_temp['code']) * [selected_date] # add date to dictionary
            if not bool(dic_main): # if dic main is empty then fill it with dict temp (only works for the first)
                dic_main = dict_temp
            else:
                for l in dic_main.keys(): # put all data into one dic main
                    dic_main[l].extend(dict_temp[l])
    driver.close() # close the driver
    
    df = pd.DataFrame(data = dic_main)
    csv_path = download_folder_path + '/' + file_name
    df.to_csv(csv_path, mode='a')
    
    return df

def soup_to_df(soup): 
    """this function turns soup into dataframe"""
    import pandas as pd
    from bs4 import BeautifulSoup
    import numpy as np
    crops = []
    codes = []
    varieties = []
    count = 0 
    for name in soup.findAll('td',attrs = {'class': "text-center"}):
        count += 1 
        if count % 3  == 1:
            codes.append(name.text)
        if count % 3  == 2:
            crops.append(name.text)
        if count % 3  == 0:
            varieties.append(name.text)

    ceilling_prices = []
    moderate_prices = []
    floor_prices = []
    count = 0 
    for price in soup.findAll('td',attrs = {'class': "text-right"}):
        count += 1 
        if count % 3  == 1:
            if price.text == '':
                ceilling_prices.append(np.nan)
            else:
                ceilling_prices.append(float(price.text))
        if count % 3  == 2:
            if price.text == '':
                moderate_prices.append(np.nan)
            else:
                moderate_prices.append(float(price.text))
        if count % 3  == 0:
            if price.text == '':
                floor_prices.append(np.nan)
            else:
                floor_prices.append(float(price.text))


    d = {'code': codes[:-1], 'crop': crops, 'variety': varieties, 
        'ceilling price': ceilling_prices,'moderate price': moderate_prices,
        'floor price': floor_prices}

    dat = pd.DataFrame(data=d)
    return dat