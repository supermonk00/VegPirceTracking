import sqlite3 as lite
import sys
con = lite.connect('vegtrack.db')
with con:
    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS DHT_data")
    cur.execute("CREATE TABLE DHT_data(timestamp DATETIME, temp NUMERIC, hum NUMERIC)")

curs = con.cursor()
# function to insert data as a table
def add_data(temp, hum):
    curs.execute("INSERT INTO DHT_data values(datetime('now'), (?), (?))",(temp,hum))
    con.commit()

add_data(20.5, 30)
add_data(25.8, 40)
add_data(30.3, 50)

print("\nEntire database contents:\n")
for row in curs.execute("SELECT * FROM DHT_data"):
    print(row)

# close the database after use
con.close()