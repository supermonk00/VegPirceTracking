import pandas as pd
import numpy as np
df = pd.read_csv('/Users/supermonk00/Desktop/VegPirceTracking/March_October_veggie_data.csv')
E = pd.Timestamp(2021,10,17)
S = E - pd.DateOffset(days=7)
S1 = S - pd.DateOffset(days=7)

# Clean up the data
df.drop('Unnamed: 0', axis=1, inplace= True)
# Replace nan in variety
df['variety'] = df['variety'].replace(np.nan, '單一品種')
# Convert messy date into datetime object
df[['Year','date_']] = df['date'].astype('str').str.split('/', n=1, expand=True)
df['Year'] = df['Year'].astype('int') + 1911
df['Date'] = (df['Year'].astype('str') +'/'+ df['date_']).astype('datetime64[ns]')
df.drop(columns=['date', 'Year', 'date_'], inplace = True)
Wn = df[df['Date'] <= E][df['Date'] >= S].groupby(['crop', 'variety','code']).mean()
Wl = df[df['Date'] <= S][df['Date'] >= S1].groupby(['crop','variety','code']).mean()
def return_rate(new, old):
    return (new - old)/old
dat = return_rate(Wn['moderate price'],Wl['moderate price'])
Toploser= dat.dropna().sort_values(ascending=True).head(10)*100  
Topgainer= dat.sort_values().sort_values(ascending=False).head(10)*100 


gain_list = dat.iloc[:].head(10).index.values
loss_list = dat.iloc[:].tail(10).index.values

mover_list = np.append(dat.iloc[:].head(5).index.values, dat.iloc[:].tail(5).index.values)

gain_values = np.round(Topgainer.values,1)
loss_values = np.round(Toploser.values,1)
mover_values = np.append(gain_values[0:5],loss_values[0:5])
mover_values = sorted(mover_values, key = abs, reverse=True)
topgainer_list = []
toploser_list = []
topmover_list = []
for i in range(10):
    topgainer_list.append(f'{gain_list[i][0]}({gain_list[i][1]})<+{str(gain_values[i])}%>')
    toploser_list.append(f'{loss_list[i][0]}({gain_list[i][1]})<{str(loss_values[i])}%>')
    topmover_list.append(f'{mover_list[i][0]}({gain_list[i][1]})<{str(mover_values[i])}%>')
