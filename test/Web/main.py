from flask import Flask, render_template
# test the plotly on html
from top_list import toploser_list, topgainer_list, topmover_list, gain_list, loss_list
from json import dumps
import plotly
# local function
import make_plot as mp



# get the data from csv

app = Flask(__name__)
@app.route('/')
def index():
    topgainer = topgainer_list
    toploser = toploser_list
    topmover = topmover_list
    gainlist = gain_list
    losslist =  loss_list
 
    return render_template('frontpage.html', 
    topgainer = topgainer, 
    toploser = toploser, topmover = topmover,
    gainlist = gainlist, losslist = losslist)


@app.route('/graphics/<name>')
def graphics(name):
    topgainer = topgainer_list
    toploser = toploser_list
    topmover = topmover_list
    fig0 = mp.make_plot(veg_code = name, duration_code = 'a1w')
    p0 = dumps(fig0, cls=plotly.utils.PlotlyJSONEncoder)
    fig1 = mp.make_plot(veg_code = name, duration_code = 'b1m')
    p1 = dumps(fig1, cls=plotly.utils.PlotlyJSONEncoder)
    fig2 = mp.make_plot(veg_code = name, duration_code = 'c3m')
    p2 = dumps(fig2, cls=plotly.utils.PlotlyJSONEncoder)
    fig3 = mp.make_plot(veg_code = name, duration_code = 'd6m')
    p3 = dumps(fig3, cls=plotly.utils.PlotlyJSONEncoder)
    fig4 = mp.make_plot(veg_code = name, duration_code = 'e1y')
    p4 = dumps(fig4, cls=plotly.utils.PlotlyJSONEncoder)
    fig5 = mp.make_plot(veg_code = name, duration_code = 'g5y')
    p5 = dumps(fig5, cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('graphics.html',  
    topgainer = topgainer, 
    toploser = toploser, 
    topmover = topmover,
    name = name,
    plot0 = p0,plot1 = p1,plot2 = p2,plot3 =p3, plot4 =p4, plot5 =p5)

@app.route('/allveg')
def charts():
    return render_template('allveg.html')

@app.route('/loadingpage')
def load():
    
    return render_template('loadingpage.html')

app.run(debug=True, port=8000)