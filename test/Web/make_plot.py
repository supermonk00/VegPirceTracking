def clean_single_market_data():
    from glob import glob
    import pandas as pd
    import numpy as np
    # get the data from csv
    df = pd.read_csv("/Users/supermonk00/Desktop/VegPirceTracking/single_market_data.csv")
    # Clean up the data
    # Replace nan in variety
    df['品種'] = df['品種'].replace(np.nan, '單一品種')
    df['品名'] = df['品名'].replace('蕃茄','番茄')


    # Convert messy date into datetime object
    df[['Year','date_']] = df['日期'].astype('str').str.split('/', n=1, expand=True)
    df['Year'] = df['Year'].astype('int') + 1911
    df['Date'] = (df['Year'].astype('str') +'/'+ df['date_']).astype('datetime64[ns]')
    df.drop(columns=['日期', 'Year', 'date_'], inplace = True)

    # Make a dict for getting whole name from code
    df['全名'] = df['品名'] + "(" + df['品種'] + ")"
    name_dict = pd.Series(df['全名'].values,index=df['品名代號']).to_dict()

    return df, name_dict

def daily_plot(veg_code, end_date, start_date):
    import plotly.graph_objects as go
    df, name_dict = clean_single_market_data()
    # Filter out specified veggitable
    df_single = df[df['品名代號'] == veg_code]
    # Count y axes range based on selected date
    df_filtered = df_single[(df_single['Date'] < end_date) & (df_single['Date'] > start_date)]
    max = df_filtered['平均價(元/公斤)'].max()
    min = df_filtered['平均價(元/公斤)'].min()
    margin = (max - min) * 0.2
    print("daily")
    # Make graph
    fig = go.Figure(go.Scatter(
        x = df_single['Date'],
        y = df_single['平均價(元/公斤)'],
    ))
    fig.update_layout(
        title = '{0}價格波動圖'.format(name_dict[veg_code]),
        xaxis_tickformat = '%d %b (%a)<br>%Y'
    )   
    # Update X and Y zoom
    fig.update_xaxes(type="date", range=[start_date, end_date])
    fig.update_yaxes(range=[min-margin, max+margin])
    return fig


def make_plot(veg_code, duration_code):
    # Convert duration code to days
    duration_to_date = {'a1w':7, 'b1m': 30, 'c3m': 90,'d6m': 180, 'e1y': 365, 'g5y': 1825 }
    import datetime
    end_date = datetime.datetime.today()
    duration = duration_to_date[duration_code]
    start_date = end_date - datetime.timedelta(duration)
    # Call to make plot
    data = daily_plot(veg_code, end_date, start_date)
    return data
