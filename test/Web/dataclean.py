def clean_single_market_data():
    import glob
    import pandas as pd
    import numpy as np
    # get the data from csv
    # df = pd.read_csv("/Users/supermonk00/Desktop/VegPirceTracking/single_market_data.csv")
    data_path = glob.glob("../../**/VegPirceTracking/single_market_data.csv", recursive=True)[0]
    df = pd.read_csv(data_path)

    # Clean up the data
    # Replace nan in variety
    df['品種'] = df['品種'].replace(np.nan, '單一品種')
    df['品名'] = df['品名'].replace('蕃茄','番茄')


    # Convert messy date into datetime object
    df[['Year','date_']] = df['日期'].astype('str').str.split('/', n=1, expand=True)
    df['Year'] = df['Year'].astype('int') + 1911
    df['Date'] = (df['Year'].astype('str') +'/'+ df['date_']).astype('datetime64[ns]')
    df.drop(columns=['日期', 'Year', 'date_'], inplace = True)

    # Make a dict for getting whole name from code
    df['全名'] = df['品名'] + "(" + df['品種'] + ")"
    name_dict = pd.Series(df['全名'].values,index=df['品名代號']).to_dict()

    return df, name_dict